
import lombok.*;

@EqualsAndHashCode
@ToString
@Getter
@Setter

public class Book {

            private String isbn;
            private Integer liczbaStron;
            private String tytul;
            private String autor;

}
