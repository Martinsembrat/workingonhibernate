package pl.sda.hibernate.task;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import pl.sda.hibernate.model.ModelCzytelnik;

@Slf4j
public class Task1 {

    //odczytać czytelnika o danym id.
    public void solve(Session session, Long id){
        ModelCzytelnik model = session.load(ModelCzytelnik.class, id);
        log.info("id = "+model.getId()
        +", login = " + model.getLogin()
        +", haslo = " + model.getHaslo());
    }
}
