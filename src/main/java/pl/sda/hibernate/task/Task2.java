package pl.sda.hibernate.task;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.hibernate.model.Kategoria;

import java.util.List;
@Slf4j
public class Task2 {

    private static final String HQL_QUERY
            = "select k from Kategoria k";
    public void solves(Session session){
        Query query = session.createQuery(HQL_QUERY, Kategoria.class);
        List <Kategoria>resultList = query.getResultList();

        log.info("Kategorie ksiązek");
        resultList.forEach(kategoria -> log.info(kategoria.toString()));
    }
}
