package pl.sda.hibernate.task;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
@Slf4j
public class Task3 {

    private  static final String HQL_QUERY ="select k.id, k.autor, k.tytul" +
            " from Ksiazka k where" +
            " k.liczbaStron >= ?1";

    public void solve(Session session, Integer liczbaStron){
        Query query = session.createQuery(HQL_QUERY);
        query.setParameter(1, liczbaStron);

        List resultList = query.getResultList();
        resultList.forEach(rekord -> {
         Object[] wartosc = (Object[]) rekord;
         log.info("id =" + wartosc[0]
         +", autor = " + wartosc[1]
         + ", tytul = " + wartosc[2]);

        });

    }
}
