package pl.sda.hibernate.model.enumeracja;

public enum NazwaKategoriiEnum {
    BIZNES,
    PORADNIKI,
    PROGRAMOWANIE,
    PROGRAMOWANIE_MOBILNE,
    WEBMASTERSTWO,
    SYSTEMY_OPERACYJNE;

}
