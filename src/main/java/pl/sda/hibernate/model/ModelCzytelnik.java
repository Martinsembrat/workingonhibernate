package pl.sda.hibernate.model;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "czytelnik")
@Getter
@Setter
public class ModelCzytelnik implements Serializable {
    @Id
    @Column (name= "id_czytelnik")
    private Long id;
    private String login;
    private String haslo;
    private String imie;
    private String nazwisko;
    private String email;
    private String telefon;


}
