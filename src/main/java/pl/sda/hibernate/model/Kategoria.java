package pl.sda.hibernate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.sda.hibernate.model.enumeracja.NazwaKategoriiEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class Kategoria {
    @Id
    @Column (name ="id_kategoria")
    private Long id;

    @Enumerated(EnumType.STRING)
    private NazwaKategoriiEnum nazwa;

    @OneToMany(mappedBy = "kategoria")
    private Set<Ksiazka> ksiazka;


}
