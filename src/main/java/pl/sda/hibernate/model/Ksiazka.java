package pl.sda.hibernate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
@Entity
@Table(name = "ksiazka")
@Getter
@Setter
@ToString
public class Ksiazka {


        @Id
        @Column(name = "id_ksiazka")
        private Long id;

        private Long id_kategoria;
        private String isbn;
        private String tytul;
        private String autor;
        @Column(name = "stron")
        private Integer liczbaStron;
        @Column(name = "rok_wydania")
        private Integer rokWydania;
        private String opis;
        private String wydawnictwo;

        @ManyToOne
        @JoinColumn(name = "id_kategoria", referencedColumnName = "id_kategoria")
        private Kategoria kategoria;



    }
