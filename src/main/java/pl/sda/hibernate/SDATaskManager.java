package pl.sda.hibernate;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.task.Task1;
import pl.sda.hibernate.task.Task2;
import pl.sda.hibernate.task.Task3;

@Slf4j
public class SDATaskManager {
    public static void main(String... args) {

        try (SessionFactory sessionFactory = new SDASessionFactory().getSessionFactory(); Session session = sessionFactory.openSession()) {
            log.info("Uzyskano obiekt sesji!");
//            Task1 task1 = new Task1();
//            task1.solve(session, 1L);
//            Task2 task2 = new Task2();
//            task2.solves(session);
              Task3 task3 = new Task3();
              task3.solve(session, 400);
        }
    }
}
